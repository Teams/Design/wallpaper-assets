import random
import xml.etree.ElementTree as ET

width = 20
height = 20
pxw = "4096"
pxh = "4096"

# https://lospec.com/palette-list/slso8
colors_all = [
    '#0d2b45',
    '#203c56',
    '#544e68',
    '#8d697a',
    '#d08159',
    '#ffaa5e',
    '#ffd4a3',
    '#ffecd6',
]

colors_dark = [
    '#0d2b45',
    '#203c56',
    '#544e68',
    '#8d697a'
]

colors_light = [
    '#d08159',
    '#ffaa5e',
    '#ffd4a3',
    '#ffecd6',
]

# https://lospec.com/palette-list/nyx8
colors_nyx8 = [
    '#08141e',
    '#0f2a3f',
    '#20394f',
    '#f6d6bd',
    '#c3a38a',
    '#997577',
    '#816271',
    '#4e495f'
]

# https://lospec.com/palette-list/dreamscape8
colors_ds8 = [
    '#c9cca1',
    '#caa05a',
    '#ae6a47',
    '#8b4049',
    '#543344',
    '#515262',
    '#63787d',
    '#8ea091'
]

# https://lospec.com/palette-list/cl8uds

colors_cloud8 = [
    '#fcb08c',
    '#ef9d7f',
    '#d6938a',
    '#b48d92',
    '#a597a1',
    '#8fa0bf',
    '#9aabc9',
    '#a5b7d4'
]

# https://lospec.com/palette-list/nostalgic-dreams
colors_dreams = [
    '#d9af80',
    '#b07972',
    '#524352',
    '#686887',
    '#7f9bb0',
    '#bfd4b0',
    '#90b870',
    '#628c70'
]

#colors = colors_dreams #day
colors = colors_ds8 #night

def sample(arr):
    return random.choice(arr)

def make_path(path_string, data=None):
    path = ET.Element('path')
    path.set('fill', sample(colors))
    path.set('d', path_string)
    if data:
        for key, value in data.items():
            path.set(key, value)
    return path

def tile(x, y, root):
    path_string = f'M {x} {y} l 1 0 a 1 1 0 0 1 -1 1 Z'
    dx = x + 0.5
    dy = y + 0.5
    rotate = 90 * (random.randint(0, 3))  # 0, 90, 180, or 270
    transform = f'translate({dx} {dy}) rotate({rotate}) translate({-dx} {-dy})'

    if random.random() < 0.9:
        # Generate square (tile) within the circle
        rectangle = ET.Element('rect')
        rectangle.set('x', str(x))
        rectangle.set('y', str(y))
        rectangle.set('width', '1')
        rectangle.set('height', '1')
        rectangle.set('fill', sample(colors))
        root.append(rectangle)
        
        path = make_path(path_string, {'transform': transform})
        root.append(path)

def generate():
    root = ET.Element('svg')
    root.set('width', pxw)
    root.set('height', pxh)
    root.set('viewBox', f"0 0 {width} {height}")
    bgrect = ET.Element('rect')
    bgrect.set('x', "0")
    bgrect.set('y', "0")
    bgrect.set('width', str(width))
    bgrect.set('height', str(height))
    bgrect.set('fill', sample(colors))
    root.append(bgrect)

    for x in range(width):
        for y in range(height + 1):
            tile(x, y, root)


    
    svg_content = ET.tostring(root).decode('utf-8')
    
    with open('geometrics.svg', 'w') as f:
        f.write(svg_content)

generate()

