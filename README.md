# Wallpaper Assets

These are the Blender project "sources" for generating the GNOME Default wallpapers. The final 
product ships in the [gnome-backgrounds](https://gitlab.gnome.org/GNOME/gnome-backgrounds) module.


